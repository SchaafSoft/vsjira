﻿using EnvDTE;
using Microsoft.VisualStudio.Shell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VSJira.Core;

namespace VSJira.Package
{
    public class VisualStudioServices : IVisualStudioServices
    {
        private readonly DTE _env;
        private readonly VSJiraPackage _package;

        public event EventHandler<OptionsChangedEventArgs> OptionsChanged;

        public VisualStudioServices(DTE environment, VSJiraPackage package)
        {
            this._env = environment;
            this._package = package;
        }

        public void ShowOptionsPage()
        {
            this._package.ShowOptionPage(typeof(JiraOptionsDialogPage));
        }

        public IJiraOptions GetConfigurationOptions()
        {
            var result = new JiraOptions();
            var properties = this._env.get_Properties(JiraOptionsDialogPage.CategoryName, JiraOptionsDialogPage.PageName);

            if (properties != null)
            {
                Object[] serverObjects;
                if (TryGetProperty(properties, "JiraServers", out serverObjects))
                {
                    result.JiraServers = serverObjects.Cast<JiraServerInfo>().ToArray();
                }

                int maxIssuesPerRequest;
                if (TryGetProperty(properties, "MaxIssuesPerRequest", out maxIssuesPerRequest))
                {
                    result.MaxIssuesPerRequest = maxIssuesPerRequest;
                }

                string openIssueMethodStr;
                VSJiraOpenIssueMode openIssueMode = VSJiraOpenIssueMode.InBrowser;
                if (TryGetProperty(properties, "OpenIssueModeHidden", out openIssueMethodStr) && Enum.TryParse<VSJiraOpenIssueMode>(openIssueMethodStr, out openIssueMode))
                {
                    result.OpenIssueMode = openIssueMode;
                }

                string themeStr;
                VSJiraTheme themeEnum = VSJiraTheme.Light;
                if (TryGetProperty(properties, "ThemeHidden", out themeStr) && Enum.TryParse<VSJiraTheme>(themeStr, out themeEnum))
                {
                    result.Theme = themeEnum;
                }
            }

            return result;
        }

        private bool TryGetProperty<T>(Properties properties, string propertyName, out T value)
        {
            value = default(T);
            try
            {
                value = (T)properties.Item(propertyName).Value;
            }
            catch
            {
                // no-op
            }

            return value != null;
        }

        public void ShowJiraIssueToolWindow(Core.UI.JiraIssueViewModel viewModel, VSJiraServices services)
        {
            this._package.ShowJiraIssueToolWindow(viewModel, services);
        }

        public void NotifyOptionsChanged(IJiraOptions options)
        {
            var args = new OptionsChangedEventArgs(options);
            var handler = this.OptionsChanged;

            if (handler != null)
            {
                handler(this, args);
            }
        }
    }
}
