﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Atlassian.Jira.Remote;

namespace VSJira.Core
{
    public class TestJiraServices :
        IIssueFieldService,
        IIssueStatusService,
        IIssuePriorityService,
        IIssueTypeService,
        IIssueResolutionService,
        IProjectService,
        IIssueFilterService
    {
        private readonly Jira _jira;

        private string[] _jiraProjects = new string[2] { "Test", "Prod" };
        private string[] _jiraStatuses = new string[2] { "Open", "In Progress" };
        private string[] _jiraPriorities = new string[2] { "High", "Low" };
        private string[] _jiraTypes = new string[2] { "Bug", "Feature" };
        private string[] _jiraResolutions = new string[2] { "Fixed", "Duplicate" };
        private string[] _jiraFilters = new string[2] { "My Open Issues", "Assigned To Me" };

        public TestJiraServices(Jira jira)
        {
            _jira = jira;
        }

        public Task<IEnumerable<CustomField>> GetCustomFieldsAsync(CancellationToken token = default(CancellationToken))
        {
            return Task.FromResult(Enumerable.Empty<CustomField>());
        }

        public Task<IEnumerable<IssueType>> GetIssueTypesAsync(CancellationToken token = default(CancellationToken))
        {
            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new IssueType(new RemoteIssueType()
                {
                    id = i.ToString(),
                    name = _jiraTypes[i]
                })));
        }

        public Task<IEnumerable<IssueType>> GetIssueTypesForProjectAsync(string projectKey, CancellationToken token = default(CancellationToken))
        {
            return this.GetIssueTypesAsync(token);
        }

        public Task<IEnumerable<IssuePriority>> GetPrioritiesAsync(CancellationToken token = default(CancellationToken))
        {
            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new IssuePriority(new RemotePriority()
                {
                    id = i.ToString(),
                    name = _jiraPriorities[i]
                })));
        }

        public Task<IEnumerable<IssueStatus>> GetStatusesAsync(CancellationToken token = default(CancellationToken))
        {
            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new IssueStatus(new RemoteStatus()
                {
                    id = i.ToString(),
                    name = _jiraStatuses[i]
                })));
        }

        public Task<IEnumerable<IssueResolution>> GetResolutionsAsync(CancellationToken token = default(CancellationToken))
        {
            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new IssueResolution(new RemoteNamedObject()
                {
                    id = i.ToString(),
                    name = _jiraResolutions[i]
                })));
        }

        public Task<IEnumerable<Project>> GetProjectsAsync(CancellationToken token = default(CancellationToken))
        {
            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new Project(_jira, new RemoteProject()
                {
                    id = i.ToString(),
                    name = _jiraProjects[i],
                    key = _jiraProjects[i].ToUpperInvariant()
                })));
        }

        public Task<Project> GetProjectAsync(string projectKey, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<JiraFilter>> GetFavouritesAsync(CancellationToken token = default(CancellationToken))
        {
            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new JiraFilter(i.ToString(), _jiraFilters[i], _jiraFilters[i])
            ));
        }

        public Task<IPagedQueryResult<Issue>> GetIssuesFromFavoriteAsync(string filterName, int? maxIssues = default(int?), int startAt = 0, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }
    }
}
