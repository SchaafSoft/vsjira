﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core
{
    public class VSJiraServices
    {
        public IJiraFactory JiraFactory { get; set; }
        public IWindowFactory WindowFactory { get; set; }
        public IVisualStudioServices VisualStudioServices { get; set; }
    }
}
