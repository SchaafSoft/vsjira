﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public class JiraCommentsPagerViewModel : PagerViewModelBase<Comment>
    {
        private readonly JiraIssueViewModel _issue;
        public ObservableCollection<Comment> Comments { get; private set; }

        public JiraCommentsPagerViewModel(JiraIssueViewModel issue, VSJiraServices services)
            : base(services)
        {
            this._issue = issue;
            this.Jira = issue.Jira;
            this.Comments = new ObservableCollection<Comment>();
        }

        public Task ResetItemsAsync()
        {
            var maxComments = this.Services.VisualStudioServices.GetConfigurationOptions().MaxIssuesPerRequest;
            return this.ResetItemsAsync(maxComments);
        }

        protected override void OnLoadingItems()
        {
            this.Comments.Clear();
            base.OnLoadingItems();
        }

        protected override async Task<IPagedQueryResult<Comment>> GetItemsAsync(int itemsPerPage, int startAt, CancellationToken token)
        {
            var comments = await this._issue.Jira.Issues.GetPagedCommentsAsync(this._issue.Key, itemsPerPage, startAt, token);

            foreach (var comment in comments)
            {
                this.Comments.Add(comment);
            }

            return comments;
        }
    }
}
