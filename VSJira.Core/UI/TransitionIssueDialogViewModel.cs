﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public class TransitionIssueDialogViewModel : ViewModelBase
    {
        private readonly VSJiraServices _services;
        private readonly JiraIssueViewModel _issue;
        private JiraNamedEntity _selectedAction;
        private string _comment;
        private string _status;

        public class DialogResult
        {
            public bool IsSucessful { get; set; }
        }

        public DialogResult Result { get; set; }
        public ObservableCollection<JiraNamedEntity> Actions { get; private set; }
        public DelegateCommand<ICloseableWindow> ExecuteCommand { get; private set; }
        public DelegateCommand<ICloseableWindow> CancelCommand { get; private set; }

        public TransitionIssueDialogViewModel(JiraIssueViewModel issue, VSJiraServices services)
        {
            this._services = services;
            this._issue = issue;
            this.Result = new DialogResult();
            this.Status = "Ready.";

            this.Actions = new ObservableCollection<JiraNamedEntity>();
            this.ExecuteCommand = new DelegateCommand<ICloseableWindow>(
                async (window) => await this.ExecuteCommandAsync(window),
                (window) => this.SelectedAction != null);

            this.CancelCommand = new DelegateCommand<ICloseableWindow>((window) => window.Close());
        }

        public VSJiraServices Services
        {
            get { return _services; }
        }

        public string Summary
        {
            get
            {
                return this._issue.Summary;
            }
        }

        public string Title
        {
            get
            {
                return String.Format("Transition Issue: {0}", this._issue.Key);
            }
        }

        public JiraNamedEntity SelectedAction
        {
            get { return _selectedAction; }
            set
            {
                _selectedAction = value;
                OnPropertyChanged("SelectedAction");
            }
        }

        public string Comment
        {
            get { return _comment; }
            set
            {
                _comment = value;
                OnPropertyChanged("Comment");
            }
        }

        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }

        public async Task LoadActionsAsync()
        {
            IEnumerable<JiraNamedEntity> actions = Enumerable.Empty<JiraNamedEntity>();
            this.Actions.Clear();

            this.Status = "Retrieving available actions...";

            try
            {
                actions = await this._issue.InternalIssue.GetAvailableActionsAsync(CancellationToken.None);
            }
            catch (OperationCanceledException)
            {
                // no-op
            }
            catch (Exception ex)
            {
                this._services.WindowFactory.ShowMessageBox("Error retrieving available actions.", ex);
            }

            foreach (var action in actions)
            {
                this.Actions.Add(action);
            }

            this.Status = "Ready.";
            this.SelectedAction = this.Actions.FirstOrDefault();
        }

        private async Task ExecuteCommandAsync(ICloseableWindow window)
        {
            var updates = new WorkflowTransitionUpdates()
            {
                Comment = this.Comment
            };

            this.Status = "Transitioning Issue...";

            try
            {
                await this._issue.Jira.Issues.ExecuteWorkflowActionAsync(
                    this._issue.InternalIssue,
                    this.SelectedAction.Name,
                    updates,
                    CancellationToken.None);

                this.Result.IsSucessful = true;
                window.Close();
            }
            catch (OperationCanceledException)
            {
                // no-op
            }
            catch (Exception ex)
            {
                this.Status = "Error: " + ex.Message;
            }
        }
    }
}
