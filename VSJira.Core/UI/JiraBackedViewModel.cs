﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public class JiraBackedViewModel : ViewModelBase
    {
        private Jira _jira;

        private readonly VSJiraServices _services;

        public JiraBackedViewModel(VSJiraServices services)
        {
            this._services = services;
        }

        public bool IsConnected
        {
            get
            {
                return this._jira != null;
            }
        }

        public Jira Jira
        {
            get
            {
                return _jira;
            }
            set
            {
                _jira = value;
                OnPropertyChanged("IsConnected");
            }
        }

        public VSJiraServices Services
        {
            get { return _services; }
        }
    }
}
