﻿using Atlassian.Jira;
using Atlassian.Jira.Remote;

namespace VSJira.Core
{
    public class TestJiraFactory : IJiraFactory
    {
        private const int DefaultTotalIssues = 500;
        private string[] _jiraProjects = new string[2] { "Test", "Prod" };
        private string[] _jiraStatuses = new string[2] { "Open", "In Progress" };
        private string[] _jiraPriorities = new string[2] { "High", "Low" };
        private string[] _jiraTypes = new string[2] { "Bug", "Feature" };
        private string[] _jiraResolutions = new string[2] { "Fixed", "Duplicate" };
        private readonly int _totalIssues;

        public TestJiraFactory(int? totalIssues = null)
        {
            this._totalIssues = totalIssues ?? DefaultTotalIssues;
        }

        public Jira Create(string url, string username, string password)
        {
            username = string.IsNullOrEmpty(username) ? "username" : username;
            password = string.IsNullOrEmpty(password) ? "password" : password;

            var client = new TestJiraRestClient();
            var jira = Jira.CreateRestClient(client, new JiraCredentials(username, password));
            var testServices = new TestJiraServices(jira);

            jira.Services.Clear();
            jira.Services.Register<IJiraRestClient>(() => new TestJiraRestClient());
            jira.Services.Register<IIssueFieldService>(() => testServices);
            jira.Services.Register<IIssueStatusService>(() => testServices);
            jira.Services.Register<IIssuePriorityService>(() => testServices);
            jira.Services.Register<IIssueTypeService>(() => testServices);
            jira.Services.Register<IIssueResolutionService>(() => testServices);
            jira.Services.Register<IProjectService>(() => testServices);
            jira.Services.Register<IIssueFilterService>(() => testServices);

            var issuesService = new TestIssueService(jira, this._totalIssues);
            jira.Services.Register<IIssueService>(() => issuesService);
            jira.Services.Register<IIssueLinkService>(() => issuesService);

            return jira;
        }
    }
}
