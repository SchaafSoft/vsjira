﻿using Atlassian.Jira;
using System;
using System.IO;
using System.Threading.Tasks;

namespace VSJira.Core
{
    public class TestWebClient : IWebClient
    {
        public Task DownloadAsync(string url, string fileName)
        {
            File.WriteAllText(fileName, url);
            return Task.FromResult(fileName);
        }

        public Task DownloadWithAuthenticationAsync(string url, string fileName)
        {
            return DownloadAsync(url, fileName);
        }
    }
}
